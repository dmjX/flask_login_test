from application import app

# Use individual models as:
# s = models.Student()
# assuming Student is a class in studentModel.py
from application.models import *

from application.config import *
from application.logic.validation import require_role

from flask import \
    render_template, \
    request, \
    url_for

# PURPOSE: Shows all the courses in a major.
@app.route('/stu/showMajorX/<string:major>', methods = ['GET'])
@require_role('students')
def showMajorX(major):
  return render_template("views/stu/showMajorXView.html", config = config)

